### Keybase proof

I hereby claim:

  * I am mircea-cosbuc on github.
  * I am mcosbuc (https://keybase.io/mcosbuc) on keybase.
  * I have a public key ASDKPA_J8U5ZkOL05gP4hDtcaVW3MDjtgGSf79Fr5uN-qQo

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "0120ca3c0fc9f14e5990e2f4e603f8843b5c6955b73038ed80649fefd16be6e37ea90a",
      "host": "keybase.io",
      "kid": "0120ca3c0fc9f14e5990e2f4e603f8843b5c6955b73038ed80649fefd16be6e37ea90a",
      "uid": "29f9be2f5bf7f9711e90b605ebeb2219",
      "username": "mcosbuc"
    },
    "merkle_root": {
      "ctime": 1589897160,
      "hash": "fe02c08d9c569d12e5a59db58adf93f4a111fe587cfccaa98719bd40cc8dfd2719be8491097d62c4a6da89844a5d4aed64402590cf57cf62d769941f9fbb8244",
      "hash_meta": "f14a19f83ea2c0500a6a770a6099981174e4fa0010231c710ea6a52bde7c61f0",
      "seqno": 16345557
    },
    "service": {
      "entropy": "verTC+XHzQpsF4eDYd3hSplX",
      "name": "github",
      "username": "mircea-cosbuc"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.4.2"
  },
  "ctime": 1589897204,
  "expire_in": 504576000,
  "prev": "da8f7663a9105606512972f6f860fb1c29a7cf9d0f4e210e4cd8e0734c25c56e",
  "seqno": 4,
  "tag": "signature"
}
```

with the key [ASDKPA_J8U5ZkOL05gP4hDtcaVW3MDjtgGSf79Fr5uN-qQo](https://keybase.io/mcosbuc), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgyjwPyfFOWZDi9OYD+IQ7XGlVtzA47YBkn+/Ra+bjfqkKp3BheWxvYWTESpcCBMQg2o92Y6kQVgZRKXL2+GD7HCmnz50PTiEOTNjgc0wlxW7EILTgLiKbIB4jRWw9AWMvAk7t3RbykeXQxeN5p0GasjwSAgHCo3NpZ8RA5Sc9zjrS1E31L94iQlJT7XTlqaf2hiTm4iYaVGHTictxuZwF4pfoVF5MGYp48UBFZOP2BvCtptiq+apFIpGVDqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIJHJUptsZ0HodF/9JMHWnXTBX1r8dkC51GWsA+KpR1bso3RhZ80CAqd2ZXJzaW9uAQ==

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/mcosbuc

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id mcosbuc
```